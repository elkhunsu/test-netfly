import React from "react";

const Banner = () => {
  return (
    <>
      <div
        className={
          "w-full h-[50vh] sm:h-[25vh] bg-cover bg-center text-white relative mb-12"
        }
      >
        <div className="absoulte w-full h-full bg-gradient-to-t from-black via-transparent to-transparent"></div>
        <div className="absolute z-10 right-0 top-1/2 transform w-[50%] md:w-[32%] left-[4%]">
          <div className="text-6xl font-bold">
            <h1>Movies</h1>
          </div>
          <div className="mt-2">
            Movies move us like nothing else can, whether they’re scary, funny,
            dramatic, romantic or anywhere in-between. So many titles, so much
            to experience.
          </div>
        </div>
      </div>
    </>
  );
};

export default Banner;
