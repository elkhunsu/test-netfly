const API_KEY = process.env.NEXT_PUBLIC_API_KEY;
export const BASE_IMG_URL = "https://image.tmdb.org/t/p/original";
export const BASE_IMG_URL_500w = "https://image.tmdb.org/t/p/w500";

const requests = {
  fetchListGenreMovie: `genre/movie/list?api_key=${API_KEY}&language=en-US`,
  fetchMoviesIds: `discover/movie?api_key=${API_KEY}&language=en-US`,
  fetchTrending: `trending/all/week?api_key=${API_KEY}&language=en-US`,
};

export default requests;
