import React, { useState, useEffect } from "react";
import Header from "../components/Header/Header";
import axios from "../axios";
import requests from "../utils/requests";
import Banner from "../components/Banner/Banner";
import RowSlider from "../components/Row/RowSlider";
import HomeFooter from "../components/HomeFooter/HomeFooter";

const Home = ({   
  genreMovies,
  trendingNow
 }) => {
  const [listMovies, setListMovies] = useState([]);

  const fetchMovies = async(array) => {
    return array.forEach(async (val, idx) => {
      if (idx >= 5) return false
      const fetch = await axios.get(`discover/movie?api_key=${process.env.NEXT_PUBLIC_API_KEY}&language=en-US&with_genres=${val.id}`).then((response) => response.data.results)
      const data = {...val, data: fetch}
      setListMovies(arr => [...arr, data])
    })
  }

  //every refresh got random genre movies
  const shuffleGenre = (array) => {
      var currentIndex = array.length, temporaryValue, randomIndex;
      while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
  }

  useEffect(() => {
    const randomGenre = shuffleGenre(genreMovies)
    fetchMovies(randomGenre)
  },[genreMovies]);

  return (
    <div className='bg-black'>
      <Header transparent={true} />
      <Banner />
      <div className='px-4'>
        <RowSlider data={trendingNow} title='Popular on Netflix' />
        {listMovies.length > 0 && listMovies.map((item) => 
            <RowSlider key={item.id} data={item.data} title={item.name} />
        )}
      </div>
      <HomeFooter />
    </div>
  );
};

export async function getStaticProps(context) {
  const [
    genreMovies,
    trendingNow
  ] = await Promise.all([
    axios.get(requests.fetchListGenreMovie).then((response) => response.data.genres),
    axios.get(requests.fetchTrending).then((response) => response.data.results),
    ]);

    return {
    props: {
      genreMovies,
      trendingNow
    },
  }
}

export default Home;
