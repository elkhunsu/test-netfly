export const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 2,
  initialSlide: 0,
  variableWidth: true,

  responsive: [
    {
      breakpoint: 1600,
      settings: {
        slidesToShow: 5,
        infinite: false,
        variableWidth: false,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        initialSlide: 0,
        infinite: true,
        arrows: false,
        variableWidth: false,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 3,
        initialSlide: 0,
        infinite: true,
        arrows: false,
        variableWidth: false,
      },
    },
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 2,
        arrows: false,
        variableWidth: true,
      },
    },
    {
      breakpoint: 480,
      settings: {
        autoplay: false,
        infinite: false,
        centerPadding: "60px",
        slidesToShow: 0.95,
        swipeToSlide: true,
        centerMode: true,
        arrows: false,
        slidesPerRow: 1,
        rows: 1,
        variableWidth: true,
      },
    },
    {
      breakpoint: 380,
      settings: {
        autoplay: false,
        infinite: false,
        centerPadding: "60px",
        slidesToShow: 0.8,
        swipeToSlide: true,
        centerMode: true,
        arrows: false,
        slidesPerRow: 1,
        rows: 1,
      },
    },
  ],
};
