import React, { useState } from "react";
import Image from "next/image";
import { BASE_IMG_URL_500w } from "../../utils/requests";

const Card = ({ movie }) => {
  const [imageError, setImageError] = useState(false);
  const fallBackSrc =
    "https://res.cloudinary.com/dewctbby3/image/upload/v1647663227/7dc497e2-4975-11ec-a9ce-066b49664af6_cm_1440w_dugogx.jpg";

  return (
    <div className="mx-1 cursor-pointer relative flex flex-wrap flex-col justify-center">
      {imageError && (
        <div className="bg-white bg-opacity-10 absolute top-0 right-0 z-50 w-full h-full"></div>
      )}
      <Image
        src={
          !imageError
            ? `${BASE_IMG_URL_500w}${movie.backdrop_path}`
            : fallBackSrc
        }
        alt="backdrop"
        className=" rounded hover:scale-105 transition-all"
        width={299}
        height={168}
        onError={() => setImageError(true)}
      />
      <div className="text-white text-xs mt-2 text-center">{movie.title}</div>
    </div>
  );
};

export default Card;
