This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## ENV key Reference

#### rename file .env-example to .env

```bash
  NEXT_PUBLIC_API_KEY = <<YOUR-API-KEY-HERE>>
```

## FAQ

#### How To get your API-KEY

register to 
https://developers.themoviedb.org/3

## Getting Started

Package Manager :

using Yarn or npm

btw, i'm using Yarn :)

First, 
```bash
npm 
# or
yarn 
```
Second, run the development server:

```bash
npm run dev
# or
yarn dev
```


Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Feedback

If you have any feedback, please reach out to us at elkin.khunsu@gmail.com

